
import '@firebase/messaging';
import '@firebase/functions';
import '@firebase/firestore';
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

admin.initializeApp(functions.config().firebase);

exports.subscribeToTopic = functions.https.onCall(async (data, _) => {
    console.log('chegou aqui');
    await admin.messaging().subscribeToTopic(data.token, data.topic);
    return `inscrito no topico ${data.topic}`;
});

exports.unsubscribeFromTopic = functions.https.onCall(async (data, _) => {
    await admin.messaging().unsubscribeFromTopic(data.token, data.topic);
    return `desinscrito do topico ${data.topic}`;
});

exports.sendOnFirestoreCreate = functions.firestore.document(
    'webpush/{pushId}'
).onCreate(async (snapshot, _) => {
    const content = snapshot.data() || { title: '', body: '', topic: '' };

    const payload: admin.messaging.Message = {
        webpush: {
            notification: {
                title: content.title,
                body: content.body,
                vibrate: [200, 100, 200],
                tag: 'pubsub',
            },
            fcmOptions: {
                link: 'https://pubsubsd.web.app'
            }
        },
        topic: content.topic || 'aula'
    };

    const ref = admin.database().ref('webPushLog');
    await ref.push(content);

    return admin.messaging().send(payload);
});