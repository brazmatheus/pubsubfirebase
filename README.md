# Referências

    https://github.com/firebase/quickstart-js/blob/master/messaging/index.html
    
# Link do Hosting

    pubsubsd.web.app
    
# Como rodar o projeto

- Instalar o firebase e o firebase-tools (pode ser necessário dar sudo em sistemas que não sejam windows);
    
```
npm i firebase -g
npm i firebase-tools -g
```

- Dar firebase login no projeto com sua conta gmaiil respectiva;
- Dar **firebase serve -p 8081**.

Caso você queira criar um projeto, siga os passos abaixo:

- Dar firebase init no projeto;
- Escolher Hosting, functions, Firestore;
- No Hosting, colocar o diretório onde será rodado o **firebase deploy --only hosting**.
- A URL do hosting é criada automaticamente a partir do *project id* do projeto criado (veja mais detalhes nos slides)

# Comandos na pasta functions
- Instalar @firebase/firestore, @firebase/functions, @firebase/messaging e @firebase/app, caso necessário. Esses npm install devem ser rodados na pasta **functions**;
- Dar npm run build, para que haja a compilação.
- O index do functions pode ser feito por typescript ou javascript. Neste projeto, optei por ts.

# Como obter par de chaves

- Configurações do projeto -> Cloud Messaging -> par de chaves.
- Colar no trecho do código:

`messaging.usePublicVapidKey('BNcExtiAQgBLL-5QvMa6QFuyUtVQLAgPBLgPyD_mrUcZMzWK-wyyXa38pyPQSbI9Baw6NvNyfCRxKrx8znYJtMc');`

# Commitar mudanças

Para cada mudança no hosting e no functions, é necessario rodar o código **firebase deploy --only *nome da feature* **

# Importar o SDK do Firebase

```
  <script src="/__/firebase/7.6.0/firebase-app.js"></script>
  <script src="/__/firebase/7.6.0/firebase-messaging.js"></script>
  <script src="/__/firebase/7.6.0/firebase-functions.js"></script>
  <script src="/__/firebase/7.6.0/firebase-firestore.js"></script>
  <script src="/__/firebase/init.js"></script>
```

# Verificar status na Notification

A função Notification.requestPermission() é disponibilizada via web API e possui três respostas: Denied, granted e defaut.

```
function requestPermission() {
      console.log('Requesting permission...');
      Notification.requestPermission().then((permission) => {
        if (permission === 'granted') {
          console.log('Notification permission granted.');
          resetUI();
        } else {
          console.log('Unable to get permission to notify.');
        }
      });
    }
```

# Regras de Cloud Firestore

Para publicar uma notificação a partir de uma mensagem criada no client, é necessário configurar regras de uso.

Database -> Cloud Firestore -> Regras

Na linha 5 do código, caso esteja if false, trocar para if true.

# Get Token

É usado quando o usuário entra em uma sessão. Ele envia o token para o server, que retorna um token único de sua sessão.

```
messaging.getToken().then((currentToken) => {
        this.tokenUsuario = currentToken;
        if (currentToken) {
          sendTokenToServer(currentToken);
          updateUIForPushEnabled(currentToken);
        } else {
          // Show permission request.
          console.log('No Instance ID token available. Request permission to generate one.');
          // Show permission UI.
          updateUIForPushPermissionRequired();
          setTokenSentToServer(false);
        }
      }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
        showToken('Error retrieving Instance ID token. ', err);
        setTokenSentToServer(false);
      });
```


# Publicar notificação

Pode ser feito: 
* Chamando uma função da biblioteca functions junto com a inserção de dados em uma tabela no firestore
* Diretamente nessa mesma tabela de firestore, só que no “Console Notification” (próximo slide)

No exemplo abaixo, a mensagem em formato de objeto “content” é inserida na tabela webpush. Caso dê erro, entra na tablea webPushLog. É necessário criar essas tabelas no console antes de usa-lás.

```
exports.sendOnFirestoreCreate = functions.firestore.document(
    'webpush/{pushId}'
).onCreate(async (snapshot, _) => {
    const content = snapshot.data() || { title: '', body: '', topic: '' };
    const payload: admin.messaging.Message = {
        webpush: {
            notification: {
                title: content.title,
                body: content.body,
                vibrate: [200, 100, 200],
                tag: 'pubsub',
            },
            fcmOptions: {
                link: 'https://pubsubsd.web.app'
            }
        },
        topic: content.topic || 'aula'
    };
    const ref = admin.database().ref('webPushLog');
    await ref.push(content);
    return admin.messaging().send(payload);
});
```

# Inscrever no tópico

Ambos seguem o mesmo contexto: A partir do token criado ao iniciar uma sessão e um nome de tópico (normalmente pré-setado por uma lógica de negócio).

```
exports.subscribeToTopic = functions.https.onCall(async (data, _) => {
    console.log('chegou aqui');
    await admin.messaging().subscribeToTopic(data.token, data.topic);
    return `inscrito no topico ${data.topic}`;
});

exports.unsubscribeFromTopic = functions.https.onCall(async (data, _) => {
    await admin.messaging().unsubscribeFromTopic(data.token, data.topic);
    return `desinscrito do topico ${data.topic}`;
});
```

# Para mais informações: 

    https://docs.google.com/presentation/d/1lTu5-Py2LmL3I57XbIKqf1ONIwmK6iBW4QIhBvQgebU/edit?usp=sharing
